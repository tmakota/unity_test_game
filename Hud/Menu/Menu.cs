﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Game;

public class Menu : MonoBehaviour {

	private bool isMenuOpen = false;
	public GUISkin mySkin;
	string[] actions = { "stone", "tree" };

	private Player player;
	private int menuImageSide, marginButon;

	void Start () {
		player = GetComponent< Player >();
		menuImageSide = Screen.height / 5;
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Escape)) toggleMenu();
	}

	void OnGUI() {
		if (isMenuOpen) {
			DrawMenu ();
			DrawItems ();
		}
	}

	private Rect GetButtonPos(int column) {
		int left = column * menuImageSide;
		return new Rect(left + menuImageSide / 10, 0 + menuImageSide / 10, menuImageSide, menuImageSide);
	}

	private void DrawItems() {
		int numActions = actions.Length;
		GUI.BeginGroup(new Rect(0, 0, Screen.width, Screen.height));
		GUI.skin.label.fontSize = menuImageSide / 6;  
		GUI.skin.button.fontSize = menuImageSide / 4; 

		for(int i = 0; i < numActions; i++) {
			Rect pos = GetButtonPos(i);
			string unitName = actions [i];
			GUI.Box(new Rect(pos.x, pos.y + menuImageSide, menuImageSide, menuImageSide), unitName);
			Texture2D action = DataManager.GetBuildImage(unitName);
			if(action) {
				if(GUI.Button(pos, action)) {
					player.prepareCreating(unitName);
					player.ResetCamera ();
					toggleMenu ();
				}
			}
		}
		GUI.EndGroup();
	}

	void DrawMenu() {
		GUI.skin = mySkin;

		float groupLeft = 0;
		float groupTop = 0;
		GUI.BeginGroup(new Rect(groupLeft, groupTop, Screen.width, Screen.height));

		//background box
		GUI.Box(new Rect(0, 0, Screen.width, Screen.height), "");

		GUI.EndGroup();
	}

	public void toggleMenu() {
		isMenuOpen = !isMenuOpen;
	}

	public bool isOpened() {
		return isMenuOpen;
	}
}
