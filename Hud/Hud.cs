﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Game;

public class Hud : MonoBehaviour {

	public Texture2D buttonHover, buttonClick;

	private int menuImageSide, marginButon;

	public Texture2D menuButton;

	private Menu menu;
	private Ground ground;

	public List<Font> fonts = new List<Font>();

	// Use this for initialization
	void Start () {
		menu = transform.parent.GetComponent< Menu >();
		ground = transform.parent.GetComponentInChildren< Ground >();
		menuImageSide = Screen.height / 6;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnGUI() {
		if (!menu.isOpened()) {
			DrawMenuButton ();
		}
	}

	private void DrawMenuButton() {

		float groupLeft = 10;
		float groupTop = 10;

		GUIStyle myStyle = new GUIStyle(); 
		myStyle.alignment = TextAnchor.MiddleCenter; 
		myStyle.normal.textColor = Color.white;

		GUI.BeginGroup(new Rect(groupLeft, groupTop, menuImageSide, menuImageSide));
		GUI.skin.label.fontSize = menuImageSide / 2;  
		GUI.skin.button.fontSize = menuImageSide / 4; 
		if(GUI.Button(new Rect(0, 0, menuImageSide, menuImageSide), menuButton)) {
			openMenu ();
		}
		GUI.EndGroup();

		GUI.BeginGroup(new Rect(menuImageSide * 2, 10, menuImageSide, menuImageSide));

		if(GUI.Button(new Rect(0, 0, menuImageSide, menuImageSide), "Greed")) {
			toggleGreed ();
		}
		GUI.EndGroup();
	}

	private void openMenu() {
		menu.toggleMenu ();
	}

	private void toggleGreed() {
		ground.toggleGreed ();
	}
}
