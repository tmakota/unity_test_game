﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Game;

public class WorldUnit : MonoBehaviour {

	public string objectName;
	public Texture2D unitImage;
	private bool isSelected = false;

	protected Bounds selectionBounds;

	protected virtual void Awake() {
		selectionBounds = DataManager.InvalidBounds;
		CalculateBounds();
	}

	void Start () {
		
	}

	void Update () {
		
	}


	public void CalculateBounds() {
		selectionBounds = new Bounds(transform.position, Vector3.zero);
		foreach(Renderer r in GetComponentsInChildren<Renderer>()) {
			selectionBounds.Encapsulate(r.bounds);
		}
	}

	public Bounds GetSelectionBounds(){
		return selectionBounds;
	}

	public void SetColliders(bool enabled) {
		Collider[] colliders = GetComponentsInChildren< Collider >();
		foreach(Collider collider in colliders) collider.enabled = enabled;
	}

	public void SetSelection(bool selected) {
		isSelected = selected;
	}

	public bool getIsSelected() {
		return isSelected;
	}

}
