﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Game;

public class Ground : MonoBehaviour {

	private bool shouldRender = true;
	public Renderer rend;

	// Use this for initialization
	void Start () {
		rend = GetComponent<Renderer>();
		rend.enabled = shouldRender;
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void toggleGreed() {
		shouldRender = !shouldRender;
		rend.enabled = shouldRender;
	}
}
