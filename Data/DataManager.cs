﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game {
	public static class DataManager {
		
		public static float ScrollSpeed { get { return 50; } }
		public static int ScrollWidth { get { return 20; } }
		public static float MaxCameraMove { get { return 50; } }
		public static float MinCameraMove { get { return -50; } }

		private static Vector3 invalidPosition = new Vector3(-99999, -99999, -99999);
		private static Bounds invalidBounds = new Bounds(new Vector3(-99999, -99999, -99999), new Vector3(0, 0, 0));
		public static Vector3 InvalidPosition { get { return invalidPosition; } }
		public static Bounds InvalidBounds { get { return invalidBounds; } }

		public static float waitTime = 0.1f;

		public static int initUnitsCount { get { return 70; } }

		private static Units gameUnitList;
		public static void SetGameObjectList(Units objectList) {
			gameUnitList = objectList;
		}

		public static GameObject GetUnit(string name) {
			return gameUnitList.GetUnitByName(name);
		}

		public static Texture2D GetBuildImage(string name) {
			return gameUnitList.GetBuildImage(name);
		}

		public static GameObject FindHitObject(Vector3 origin) {
			Ray ray = Camera.main.ScreenPointToRay(origin);
			RaycastHit hit;
			if(Physics.Raycast(ray, out hit)) return hit.collider.gameObject;
			return null;
		}

		public static Vector3 FindHitPoint(Vector3 origin) {
			Ray ray = Camera.main.ScreenPointToRay(origin);
			RaycastHit hit;
			if(Physics.Raycast(ray, out hit)) return hit.point;
			return DataManager.InvalidPosition;
		}
	

	}
}