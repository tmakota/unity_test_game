﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Game;

public class Units : MonoBehaviour {

	public GameObject[] units;
	string[] unitsNames = { "stone", "tree" };
	string[] sides = { "left", "top", "right", "bottom" };

	private Player player;

	void Awake() {
		DataManager.SetGameObjectList(this);
		player = transform.parent.GetComponent< Player >();
	}

	void Start () {
		generateRandowUnits ();
	}

	void Update () {

	}

	private void generateRandowUnits() {
		string unitName;
		string side;

		for(int i = 0; i < DataManager.initUnitsCount; i++) {
			unitName = unitsNames.RandomItem();
			side = sides.RandomItem ();
			player.addItem (unitName, side);
		}
	}


	public GameObject GetUnitByName(string name) {
		for(int i = 0; i < units.Length; i++) {
			WorldUnit unit = units[i].GetComponent< WorldUnit >();
			if(unit && unit.name == name) return units[i];
		}
		return null;
	}

	public Texture2D GetBuildImage(string name) {
		for(int i = 0; i < units.Length; i++) {
			WorldUnit unit = units[i].GetComponent< WorldUnit >();
			if(unit && unit.name == name) return unit.unitImage;
		}
		return null;
	}
}

public static class ArrayExtensions
{
	public static T RandomItem<T>(this T[] array)
	{
		return array[Random.Range(0, array.Length)];
	}
}
