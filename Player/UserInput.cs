﻿using UnityEngine;
using System.Collections;

using Game;

public class UserInput : MonoBehaviour {

	private Player player;
	public string errorMessage = "msg";


	private float timeSinceClickDetected = 0.0f;

	void Start () {
		player = transform.root.GetComponent< Player >();
	}

	void Update () {
		if(player) {
			if (Input.touchCount > 0)
			{
				LeftMouseClick (true);
			}
			if (Input.GetMouseButtonDown (0)) {
				LeftMouseClick (false);
			}
			if(Input.GetMouseButton(0)) {
				player.MoveCamera();
			}
		}
		timeSinceClickDetected += Time.deltaTime;
	}

	void OnGUI() {
		GUI.BeginGroup(new Rect(0, 0, Screen.width, Screen.height));
		GUI.contentColor = Color.black;
		GUI.Label(new Rect(10, Screen.height - 50, Screen.width, 40), errorMessage);
		GUI.EndGroup();
	}

	private void LeftMouseClick(bool isTouch) {
		GameObject hitObject = DataManager.FindHitObject(Input.mousePosition);
		Vector3 hitPoint = DataManager.FindHitPoint(Input.mousePosition);
		if (hitObject && hitPoint != DataManager.InvalidPosition) {
			Placeholder worldObject = hitObject.transform.GetComponent< Placeholder > ();
			if (worldObject && worldObject.objectName == "Placeholder") {
					player.PlaceItem (worldObject.transform.position);
					Destroy (hitObject);
			}
			if (hitObject.name == "Ground") {
				Placeholder placeholder = player.getPlaceholder ();
				if (placeholder) {
					placeholder.setNewPosition (hitPoint);
				}
			}
		} else {
			if (isTouch) {
				SingleTouch ();
			}
		}
	}

	private void SingleTouch() {
		Vector3 clickPosition = DataManager.InvalidPosition;
		bool clickDrag = false;

		Touch touch = Input.GetTouch(0);
		if (touch.phase == TouchPhase.Began) {
			timeSinceClickDetected = 0.0f;
		}
		if (touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Stationary) {
			if (timeSinceClickDetected > 0.9 * DataManager.waitTime) {
				clickDrag = true;
				clickPosition = new Vector3(touch.position.x, touch.position.y, 0.0f);
			}

		}
		else if(touch.phase == TouchPhase.Ended) {
			clickDrag = false;
		}

		if(clickPosition == DataManager.InvalidPosition) return;

		if (clickDrag) {
			float xpos = clickPosition.x;
			float ypos = clickPosition.y;

			Vector3 movement = new Vector3 (0, 0, 0);

			if (ypos > Screen.height / 4 && ypos < (Screen.height - Screen.height / 4)) {
				if (xpos < Screen.width / 2) {
					movement.x -= DataManager.ScrollSpeed;
				} else {
					movement.x += DataManager.ScrollSpeed;
				}
			}

			if (ypos < Screen.height / 2) {
				movement.z -= DataManager.ScrollSpeed;
			} else {
				movement.z += DataManager.ScrollSpeed;
			}

			player.MoveToDestination (movement);
		}
	}
}
