﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Game;

public class Player : MonoBehaviour {

	public string username;
	private string itemToPlace;
	private Placeholder placeholder;

	private WorldUnit tempBuilding;
	private bool findingPlacement = false;

	void Start () {

	}

	// Update is called once per frame
	void Update () {
		if(findingPlacement) {
			tempBuilding.CalculateBounds();
		}
	}

	public void setPlaceholder(Placeholder item){
		placeholder = item;
	}

	public Placeholder getPlaceholder(){
		return placeholder;
	}

	public void prepareCreating(string unitName) {
		itemToPlace = unitName;
		Vector3 position = new Vector3 (0, 0.3f, 0);
		GameObject prefab = DataManager.GetUnit ("Placeholder");
		Quaternion rotation = new Quaternion ();
		GameObject place = Instantiate (prefab, position, rotation);
		setPlaceholder (place.GetComponent<Placeholder>());
	}

	public void PlaceItem(Vector3 hitPoint) {
		GameObject prefab = DataManager.GetUnit (itemToPlace);
		Vector3 position = hitPoint;
		Quaternion rotation;
		if (itemToPlace == "stone") {
			rotation = new Quaternion ();
		} else {
			rotation = Quaternion.Euler(90f * -1,0,0);
		}
		Instantiate (prefab, position, rotation);
	}

	public void addItem(string unitName, string side) {
		Vector3 position;
		GameObject prefab;
		Quaternion rotation;

		float randomX;
		float randomZ;

		float min = DataManager.MinCameraMove + 2;
		float max = DataManager.MaxCameraMove - 2;


		switch(side) {
			case "top":
				randomX = Random.Range(min, max);
				randomZ = Random.Range(min, min + 2);
			break;
			case "right":
				randomX = Random.Range(max - 2, max);
				randomZ = Random.Range(min, max);
			break;
			case "bottom":
				randomX = Random.Range(min, max);
				randomZ = Random.Range(max - 2, max);
			break;
			default:
				randomX = Random.Range(min, min + 2);
				randomZ = Random.Range(min, max);
			break;
		}

		prefab = DataManager.GetUnit (unitName);
		if (unitName == "stone") {
			rotation = new Quaternion ();
		} else {
			rotation = Quaternion.Euler(90f * -1,0,0);
		}
		position = new Vector3 (randomX, 0.3f, randomZ);

		Instantiate (prefab, position, rotation);
	}

	public void MoveCamera() {
		float xpos = Input.mousePosition.x;
		float ypos = Input.mousePosition.y;
		Vector3 movement = new Vector3(0,0,0);

		//horizontal camera movement
		if(xpos >= 0 && xpos < DataManager.ScrollWidth) {
			movement.x -= DataManager.ScrollSpeed;
		} else if(xpos <= Screen.width && xpos > Screen.width - DataManager.ScrollWidth) {
			movement.x += DataManager.ScrollSpeed;
		}

		//vertical camera movement
		if(ypos >= 0 && ypos < DataManager.ScrollWidth) {
			movement.z -= DataManager.ScrollSpeed;
		} else if(ypos <= Screen.height && ypos > Screen.height - DataManager.ScrollWidth) {
			movement.z += DataManager.ScrollSpeed;
		}

		MoveToDestination (movement);
	}

	public void ResetCamera() {
		Vector3 movement = new Vector3(0,10,-20);
		Camera.main.transform.position = movement;
	}

	public void MoveToDestination(Vector3 movement) {

		Vector3 origin = Camera.main.transform.position;
		Vector3 destination = origin;
		destination.x += movement.x;
		destination.z += movement.z;

		if(destination.x > DataManager.MaxCameraMove) {
			destination.x = DataManager.MaxCameraMove;
		} else if(destination.x < DataManager.MinCameraMove) {
			destination.x = DataManager.MinCameraMove;
		}

		if(destination.z > DataManager.MaxCameraMove) {
			destination.z = DataManager.MaxCameraMove;
		} else if(destination.z < DataManager.MinCameraMove) {
			destination.z = DataManager.MinCameraMove;
		}

		if(destination != origin) {
			Camera.main.transform.position = Vector3.MoveTowards(origin, destination, Time.deltaTime * DataManager.ScrollSpeed);
		}

	}
}
